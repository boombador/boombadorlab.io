let goatCounterHandle = null;

// TODO: should return empty string if invalid path (doesn't start with posts, basically)
const hashedRelativePath = (hash) => hash.replace(/^\#\//, "");

const canonicalPath = (location) => {
  const { pathname, search, hash } = location;
  return pathname + hashedRelativePath(hash) + search;
};

// goatPath : location.pathname + location.search (as string)
// livePath : location.origin + location.pathname + location.search + location.hash
// canonicalPath : location.pathname + hashedRelativePath(location.hash) + location.search

export const convertToCanonical = (goatPath) => {
  const goatPathname =
    goatPath.indexOf("?") >= 0 ? goatPath.split("?")[0] : goatPath;
  const pathname =
    goatPathname === "/"
      ? goatPathname + hashedRelativePath(window.location.hash)
      : goatPathname;
  return pathname;
};

const livePath = () => location.pathname + location.search + location.hash;

const saveHandle = (goatCounter) => {
  goatCounterHandle = goatCounter;
};

const triggerPageview = (defaultArgs = undefined) => {
  waitForHandleLoaded(() => {
    goatCounterHandle.count(defaultArgs);
  });
};

const goatCounterLoaded = () =>
  !!(window.goatcounter && window.goatcounter.count);

const waitForHandleLoaded = (afterLoaded) => {
  if (goatCounterLoaded()) {
    afterLoaded();
    return;
  }

  // poll until goatcounter lib is loaded
  const analyticsSetupInterval = setInterval(function () {
    if (goatCounterLoaded()) {
      clearInterval(analyticsSetupInterval);
      saveHandle(window.goatcounter);
      afterLoaded();
    }
  }, 100);
};

const initialLoadTracking = () => {
  // it appears that when goatcounter initializes it looks at the value
  // at `window.goatcounter` for initial configuration
  // assuming goatcounter isn't already loaded by the time this function gets
  // invoked because the script tag has the async attribute (unreliable)
  window.goatcounter = {
    allow_local: debugAnalyticsLocal, // library just emits a warning when false, needed to test path rewriting
    no_onload: false, // must be false for the `path` rewrite function to be invoked
    path: convertToCanonical,
  };

  triggerPageview({ path: canonicalPath(window.location) });
  window.addEventListener("hashchange", (__event) => triggerPageview());
};

export default initialLoadTracking;
