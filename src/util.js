import queryString from "query-string"; // possible alternative: URLSearchParams

// https://stackoverflow.com/questions/641857/javascript-window-resize-event
export const addEvent = (object, type, callback) => {
  if (object == null || typeof object == "undefined") return;
  if (object.addEventListener) {
    object.addEventListener(type, callback, false);
  } else if (object.attachEvent) {
    object.attachEvent("on" + type, callback);
  } else {
    object["on" + type] = callback;
  }
};

// courtesy of: https://gist.github.com/mathewbyrne/1280286
export const slugify = (text) =>
  text
    .toString()
    .toLowerCase()
    .replace(/\s+/g, "-") // Replace spaces with -
    .replace(/[^\w\-]+/g, "") // Remove all non-word chars
    .replace(/\-\-+/g, "-") // Replace multiple - with single -
    .replace(/^-+/, "") // Trim - from start of text
    .replace(/-+$/, ""); // Trim - from end of text

export const permalink = (post) => {
  return `/post/${slugify(post.title)}`;
};

export const queryWindowDraftsRequested = () => {
  const [_, queryParams] = window.location.hash.split("?");
  const { drafts } = queryString.parse(queryParams);
  return drafts === "assimilated";
};

export const shouldIncludeDrafts = (routeLocation) => {
  const location = routeLocation || window.location;
  const { drafts } = queryString.parse(location.search);
  return drafts === "assimilated";
};
