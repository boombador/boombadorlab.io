---
started: 2019-05-01T15:05:50-0400
date: 2019-05-16T21:05:00-0400
title: Creating Art with Blender 2.80, Scripting and Sampling
layout: basic
language: python
---

Programming graphics can be satisfying yet maddening. You have fun visual feedback balanced
against debugging code that in some instances is basically a physics simulation of light, however it
can be quite rewarding to create a window into a virtual world that was born in your head. One
popular tool for authoring images or 3D models is the desktop application
[Blender].

Blender is free and [open source] software that provides a powerful API for manipulating scene data
either through a customizable gui with an emphasis on hotkeys or a bundled python runtime and shell.
Think of it as a mix between gimp, vim, and delectable 3D goodness. I've found it useful for making
images, 3D prints, and models of the physical world for analysis. I like it very much.

This post will walk through generating a scene by sampling return values from a custom function and
then using another custom function to convert those values into geometric objects. More concretely,
our example custom functions will create pillars with their height based on sine and cosine waves.

<img src="/img/sampling-render.jpg">

While I haven't used Blender much for a couple of years, the recent 2.80 beta release of the
software has prompted me to take another look.  All of the [code for this post][link to procgen] is
available on github ready for you to build off of. Some parts, which I will try to point out, would
need to change to run on the current stable Blender version of 2.79.

# How to Follow Along

If you'd like to follow along you will need to
[download Blender 2.80 (beta)](https://builder.blender.org/download/), which is available for
Windows, Mac and Linux.

After you have installed and started up the Blender application you will be greeted by the default
scene along with a menu card for opening recent files or new file templates. Click anywhere outside
of the file opener menu to dismiss it then click the scripting tab on the top menu bar.

<img src="/img/startup.jpg">

This will set up your workspace with a layout optimized for scripting.
Using the factory defaults from the 2.80 beta resulted in windows for a very simple text editor,
a 3D viewport for visually inspecting our scene, a python shell with the blender module `bpy`
already imported, and an info window which echoes the python api counterpart to the various gui
actions you perform with the mouse and keyboard shortcuts, which can be extremely helpful. I usually
edit my script externally with vim so I made the editor window very small here.

<img src="/img/scripting.jpg">

We'll be typing all the code listings directly into the python shell exposed by blender, which will
update state in the bundled python runtime just like a python shell in your terminal.

If you want to follow along you may want to enter each snippet as it appears, since later examples will
build on earlier definitions. Keep in mind that blank lines will interrupt any multiline
definitions in the shell environment so avoid non-essential blank lines.

My current development process is awkward but so far manageable: I keep my script open
in a file where I edit it and paste changed definitions into the blender python shell. Periodically
I refresh the blender file with the `<ctrl+n>` shortcut and paste the latest version of my script
into a fresh python shell. I think packaging code as an addon might make this easier to manage, so I
will likely explore that approach.

# Dealing with Meshes

The python api for blender appears to be a thin wrapper around the core C++ engine, which makes sense
since the project seems to be under heavy development, but as a result the interface seems to favor
the ergonomics of clicking objects on the screen instead of traversing a scene graph as is necessary
in scripts. So we'll start by defining some python functions to abstract away the stateful nature of
the blender operators we'll be relying on. 

These utility functions will take a snapshot of any global state that may be modified
by the operator and restore it after the operation completes, hiding details which could make
reasoning about the bigger picture more difficult.

First we define `add_cube` to create and return a reference to a new mesh containing cube data.
When the operator is run the reference to the active object in the scene is updated to point to the
new cube mesh, but then we overwrite the active object with whatever it previously pointed to.

    def add_cube():
        saved_object = bpy.context.object
        bpy.ops.mesh.primitive_cube_add()
        created_mesh = bpy.context.object
        bpy.context.view_layer.objects.active = saved_object
        return created_mesh

Note that we are reading the active object from `bpy.context.object` but updating it by overwriting
`bpy.context.view_layer.objects.active`. The `view_layer` is a new interface introduced in the 2.80
version so I'm not terribly familiar with it yet, however it passes that essential bar in side
projects of "seems to work".

With a bit of indirection we can keep this simple api for any operators that we care to provide
an alias for in a mapping called `add_mesh_functions`:

```
add_mesh_functions = {
        'cube': 'primitive_cube_add',
        'sphere': 'primitive_uv_sphere_add'}


def add_mesh(mesh_name, add_operator_opts = None):
    saved_object = bpy.context.object
    create_mesh_func_name = add_mesh_functions.get(mesh_name)
    if not create_mesh_func_name:
        return None
    if not add_operator_opts:
        add_operator_opts = {}
    create_mesh_func = getattr(bpy.ops.mesh, create_mesh_func_name)
    create_mesh_func(**add_operator_opts)
    created_mesh = bpy.context.object
    bpy.context.view_layer.objects.active = saved_object
    return created_mesh
```

Next we will define a function that helps us modify an object's origin, a concept closely related to
coordinate systems. The origin of an object is the point of reference for any point in the object's
coordinate system, and when we set the location of an object we set are moving it so
its origin matches the requested coordinates.

    def set_object_origin(obj=None, new_origin=None):
        if obj and new_origin:
            saved_location = bpy.context.scene.cursor_location
            saved_object = bpy.context.object
            bpy.context.view_layer.objects.active = obj
            bpy.context.scene.cursor_location = Vector(new_origin)
            bpy.ops.object.origin_set(type='ORIGIN_CURSOR')
            bpy.context.scene.cursor_location = saved_location
            bpy.context.view_layer.objects.active = saved_object

For example, imagine a sphere of radius 1 with its origin at its center. If we
were modeling a scene where the floor is on the x-y plane and we want to set the ball on the
ground, our first thought might be to set the vertical coordinate to zero with a location such
as `(0,0,0)`, but in this case half of the sphere will overlap with the ground like so:

<img src="/img/origin_sphere_collide.jpg">

But if the mesh is defined such that the origin is at the bottom of the sphere, we can
more conveniently position the object in terms of its position in the containing scene.
Assume the example sphere mentioned above is stored in the variable `sphere_obj`, then
we can adjust the origin and position the sphere.

    >>> sphere_obj = add_mesh('sphere')
    >>> set_object_origin(sphere_obj, new_origin=(0, 0, -1))
    >>> sphere_obj.location = (0, 0, 0)

<img src="/img/origin_sphere_contact.jpg">

Similarly we will wrap the functionality for applying the transformations associated with
an object. In blender when you double the scale of an object, the coordinates of its vertices
in the original object don't change but instead when calculating the final scene every element
is adjusted by the transformations associated with the child-parent  relationship in the scene.

`set_object_transform_apply` will apply those transformations for location, rotation and
scale down to the objects themselves, meaning the vertices will have their coordinates updated
so that they remain in position as the transform values are reset to their defaults.

    def set_object_transform_apply(obj):
        saved_object = bpy.context.object
        bpy.context.view_layer.objects.active = obj
        bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)
        bpy.context.view_layer.objects.active = saved_object

Example usage:

    >>> cube_obj = add_cube()
    >>> set_object_origin(cube_obj, new_origin=(0, 0, -1))
    >>> cube_obj.location = (0, 0, 0)  # now top of cube is 2 units above x-y plane 
    >>> cube_obj.scale = (2,2,2)
    >>> set_object_transform_apply(cube_obj)

Add a helper function to create a material with the requested color and attach
it to a given object:

    def add_color_to_obj(obj, color):
        mat = bpy.data.materials.new(name="{}-mat".format(obj.name))
        obj.data.materials.append(mat)
        obj.active_material.diffuse_color = color

Finally we'll create a helper to streamline defining the common attributes of the
pillars in the scene. This returns a scene object containing a mesh for a pillar
with the origin at the base.

```
def add_pillar(x, y, dx, dy, dz):
    # default cube is axis-aligned of side length 2
    obj = add_mesh('cube', {'location': (x, y, 1)})
    set_object_origin(obj, new_origin=(x, y, 0))
    obj.scale.x = dx / 2
    obj.scale.y = dy / 2
    obj.scale.z = 1
    set_object_transform_apply(obj)
    obj.scale.z = dz
    return obj
```

We treat the z scale differently because we are going to be using that transformation value to
set the height of the pillars at a later step. Try adding some pillars to your scene and
experimenting with their attributes in the GUI.

# Sampling Framework

We will write some scaffolding code that will allow us to decouple the logic of taking samples in
our desired input space from the logic of how we want each sample to contribute to the overall scene.
We'll start with a couple of classes to help get organized.

We start by implementing a `Sample` class which encapsulates information about what
the function returned and on what coordinates it was invoked, as well as any
extra information we need to inject for rendering a sample.

    class Sample:
        def __init__(self, x, y, sampling_func, sampling_context, render_context):
            self.x = x
            self.y = y
            if 'i' in render_context or 'j' in render_context:
                i = render_context.pop('i', 'unknown')
                j = render_context.pop('j', 'unknown')
                render_context['name'] = "pillar-{}-{}".format(i, j)
            self.render_context = render_context
            self.value = sampling_func(x, y, sampling_context = sampling_context)

The Bounds class defines the range of possible values that will be fed into our function.
Specifically we will be dealing with an axis-aligned region on the x-y plane,
which we define by storing the minimal and maximal coordinates for each dimension: 

    class Bounds:
        def __init__(self, x_range = None, y_range = None):
            if not (x_range and y_range):
                raise NotImplemented
            self.x = x_range
            self.y = y_range

How to go about sampling can be a complicated science, but we'll keep it simple
and calculate a regularly spaced grid of points. We'll create a couple helper
functions to calculate the spacing between our sample points and the indices of
the points we're visiting.

    def step_length(bounds_dimension, sample_factor):
        """ length of a single step in the sample """
        x0 = bounds_dimension[0]
        x1 = bounds_dimension[1]
        side_length = abs(x1 - x0)
        return side_length / sample_factor

    def sample_indices(sample_factor): # dimensions = 2
        """ assuming dimension two (i, j) """
        for i in range(sample_factor):
            for j in range(sample_factor):
                yield i, j
        return None

    def generate_samples(sampling_func, sampling_context = None, bounds = None, sample_factor = 2):
        if not bounds:
            bounds = Bounds([-1, 1], [-1, 1])
        dx = step_length(bounds.x, sample_factor)
        dy = step_length(bounds.y, sample_factor)
        x0 = bounds.x[0] + (0.5 * dx)
        y0 = bounds.y[0] + (0.5 * dy)
        for i, j in sample_indices(sample_factor):
            x = x0 + dx * i
            y = y0 + dy * j
            yield Sample(
                    x, y, sampling_func, sampling_context,
                    {'dx':dx, 'dy':dy, 'i':i, 'j':j})
        return None

# Generating the Scene

Now we will use the `generate_samples` function to map points in the bounded sampling space to the
values of the given sampling function. Iterating over the returned samples lets you focus on writing
rendering code for a single sample in the body of the loop.

For our sampling function we'll start with a something that plugs the x and y coordinates
into the sine and cosine functions respectively, then adds a configurable vertical offset for
positioning. Trigonometric functions tend to be nice debugging tools for situations like this
because the output is regular on an infinite domain so you can often use it to spot unintended
transformations with little thought given to how you're calling it.

    import math

    def combine_sin_cos(x, y, sampling_context = None):
        return math.sin(x) + math.cos(y) + sampling_context.get('offset', 0)
    
    def value_to_color(v):
        half_range = 1.5
        t = (v + half_range) / (2 * half_range)
        green = t
        red = 1 - t
        return (red, green, 0, 1)

Now that we've decided what render context we'll inject in the sample we can write a helper to
convert each sample to a pillar object. 

    def add_pillar_from_sample(sample):
        name, dx, dy = (
                sample.render_context.get('name'),
                sample.render_context.get('dx'),
                sample.render_context.get('dy'))
        obj = add_pillar(sample.x, sample.y, dx, dy, sample.value)
        obj.name = name
        add_color_to_obj(obj, value_to_color(sample.value))
        return obj

And finally we can tie it all together in our demo function.

    def add_sampled_pillars(sample_factor = 5):
        bounds = Bounds([-4, 4], [-4, 4])
        context = {'offset': 0}
        for sample in generate_samples(
                combine_sin_cos,
                sampling_context=context,
                bounds=bounds,
                sample_factor=sample_factor):
            add_pillar_from_sample(sample)

You can see the smooth patterns start to appear more clearly with a higher sample factor, but if too
high then blender will freeze while it tries to crunch the numbers, since the computation cost scales
exponentially with the `sample_factor`.

    add_sampled_pillars(sample_factor=10)

# Doing a Simple Render

Here are some streamlined instructions for generating an image of your scene. These won't go into
topics light like lighting setup or image composition but rather create a minimal setup with
simulated sunlight and a camera position based on your 3D preview.

Press `<shift+a>` to bring up the 'Add' menu, then select Camera to add one to the scene. Rotate the
3D viewport until you roughly have the angle you want for your shot, then press `<ctrl+alt+numpad 0>`
to snap the camera to match your preview.

You can move the camera back and forth along the line of sight by pressing `g` once with the camera
selected to enter grab mode then click the scroll wheel and move the mouse around to adjust. Press
enter or left click the mouse to confirm your update.

Press `<shift+a>` to bring up the 'Add' menu again and select a Sun under the Light options. You can
rotate this slightly by pressing `r` right after adding it to change the shadow angle and make it more
noticeable.

Finally press F12 to trigger a render from the perspective of the scene camera, then under the Image
menu select 'Save As' to pick a filename.

# Next Steps

There are many other [mesh operators] you can play around with, as well as approaches for exploring
such as generating multiple meshes per sample or more significantly modifying based on the sample value.
Here is an example sampling the same function but rendering spheres with varying radii:

<img src="/img/sampling-render-spheres.jpg">

And the code, which closely mirrors the existing pillar logic:

```
def add_orb(x, y, radius, value):
    """ generate a sphere of the given radius and scale to the value """
    obj = add_mesh('sphere', {
        'location': (x, y, 0),
        'radius': radius})
    value = value / 1.3
    obj.scale.x = value
    obj.scale.y = value
    obj.scale.z = value
    return obj

def add_orb_from_sample(sample):
    name, radius = (
            sample.render_context.get('name'),
            sample.render_context.get('dx'))
    radius = 1
    obj = add_orb(sample.x, sample.y, radius, sample.value)
    obj.name = name
    add_color_to_obj(obj, value_to_color(sample.value))
    return obj

def add_sampled_orbs(sample_factor = 5):
    bounds = Bounds([-4, 4], [-4, 4])
    for sample in generate_samples(
            combine_sin_cos,
            bounds=bounds,
            sample_factor=sample_factor):
        add_orb_from_sample(sample)
```

When the sampling factor is turned up to 10 it gives me fairly different impression,
which is part of what's so interesting about these generative parameter spaces for me.

<img src="/img/sampling-render-spheres-10.jpg">

Thank you for reading! I hoped you enjoyed learning about these topics as much as I did. I plan to
keep playing around with scripting in Blender and probably iterating on this framework as a way to
experiment with the api.

What images do you come up with if you change the sampling function or experiment with rendering
different meshes? There are other operators for adding basic meshes, or you could get more advanced
and manipulate individual vertices.

[Blender]: https://www.blender.org/
[open source]: https://developer.blender.org/diffusion/B/
[link to procgen]: https://github.com/boombador/blender-procgen/blob/master/posts/art-blender-intro.py
[blender docs]: https://wiki.blender.org/wiki/Main_Page
[mesh operators]: https://docs.blender.org/api/blender2.8/bpy.ops.mesh.html