import * as THREE from 'three';

import post from './post.md';
import vertexShader from './vertex-shader.glsl';
import fragmentShader from './fragment-shader.glsl';

var renderDiv, renderer, scene, camera, mesh;
var start = Date.now();
var fov = 30;
var WIDTH = 400, HEIGHT = 300;
var material;

window.addEventListener( 'load', init );

function init() {
  renderDiv = document.querySelector('.canvasInPost');
  if (!renderDiv) {
    // not on the specific post page
    // TODO: figure out how to conditionally load this
    return;
  }
  renderDiv.innerHTML = null;
  scene = new THREE.Scene();

  camera = new THREE.PerspectiveCamera( fov, WIDTH / HEIGHT, 1, 10000 );
  camera.position.z = 100;
  camera.target = new THREE.Vector3( 0, 0, 0 );

  scene.add( camera );

  material = new THREE.ShaderMaterial( {
    uniforms: { 
      tExplosion: { type: "t", value: THREE.ImageUtils.loadTexture( '/img/electric_blue.png' ) },
      time: { type: "f", value: 0.0 },
      weight: { type: "f", value: 10.0 }
    },
    vertexShader,
    fragmentShader,
  });

  mesh = new THREE.Mesh( new THREE.IcosahedronGeometry( 20, 5 ), material );
  scene.add( mesh );

  renderer = new THREE.WebGLRenderer();
  renderer.setSize( WIDTH, HEIGHT );
  renderer.autoClear = false;

  renderDiv.appendChild( renderer.domElement );

  window.addEventListener( 'resize', onWindowResize, false );

  render();
  console.log('initialized perlin shader post');
}

function onWindowResize() {
  renderer.setSize( WIDTH, HEIGHT );
  camera.projectionMatrix.makePerspective( fov, WIDTH / HEIGHT, 1, 1100 );
}

var onMouseDownMouseX = 0, onMouseDownMouseY = 0,
    lon = 0, lat = 0, phi = 0, theta = 0, lat = 15, scale = 0;

function render() {

  material.uniforms[ 'time' ].value = .00025 * ( Date.now() - start );

  scale += .005;
  scale %= 2;

  lat = Math.max( - 85, Math.min( 85, lat ) );
  phi = ( 90 - lat ) * Math.PI / 180;
  theta = lon * Math.PI / 180;

  camera.position.x = 100 * Math.sin( phi ) * Math.cos( theta );
  camera.position.y = 100 * Math.cos( phi );
  camera.position.z = 100 * Math.sin( phi ) * Math.sin( theta );

  camera.lookAt( scene.position );
  renderer.render( scene, camera );
  requestAnimationFrame( render );
}

export default post;
