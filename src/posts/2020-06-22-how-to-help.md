---
title: How to help with racial justice
date: 2020-06-22T00:00:00-04:00
layout: basic
draft: true
---

I don't know what the answer is here. I am in the process of studying

- voting : exert selective pressure
- protesting : risky with the coronavirus, but does have an impact. people seem less likely to support beating on innocents.
- contacting representatives digitally or by mail: Can make most impact if you are informed and focus on the human impact, or you are bringing up a niche issue that could help people but isn't ideologically charged. Hard to move people when they've very publicly made it their hill to die on.
- 8cantwait : reduce likelihood of risk, in the US there were [an estimated 1,098](https://mappingpoliceviolence.org/). Our police-initiated deaths per 10 million for that year was 46, substantially higher than other first world nations https://en.wikipedia.org/wiki/List_of_killings_by_law_enforcement_officers_by_country.
- oversight boards
- defund the police : Reduce the budget of police forces, perhaps to very small levels, and spend that money on social services to prevent situations requiring police intervention. I support reversing the hyper-militarization of the police, it seems clear that such a power imbalance would
- abolish the police : 
