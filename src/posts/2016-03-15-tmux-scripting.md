---
title: My Tmux Notes
date: 2016-03-15T00:00:00-04:00
layout: basic
language: bash
---

I find tmux to be a very helpful tool for setting up an environment specific to the project 
I'm working on in a reproducible manner. You can start a named tmux session with the `-s` flag:

    $ tmux new -s blogging

Then create a file `.tmuxrc` with a list of tmux commands, usually these would be prefixed with the
`tmux` root command itself:

    rename-session blog
    rename-window build
    send "jekyll serve --watch --host 0.0.0.0" C-m
    split-window -h
    send "ifconfig | grep 'inet addr'" C-m
    new-window -n vim
    send "vim" C-m

From within the tmux shell you can source the config file you just created in order to apply those
commands:

    $ tmux source-file /path/to/.tmuxrc

An alternative that probably makes more sense would be to initialize the tmux session from a bash
script directly, its just as easy to send commands to the tmux server but you can perform setup if
necessary:

    allowed_host="0.0.0.0"
    tmux start-server
    tmux new-session -d -s blog build
    tmux send "jekyll serve --watch --host $allowed_host" C-m
    tmux split-window -h
    tmux send "ifconfig | grep 'inet addr'" C-m
    tmux new-window -n vim

Interesting fact from the whiteboardcoder site, the format of the `-t` flag is [session]:[window].[pane],
so to create a new session with two horizontal panes on the first window and send a command to tbe
bottom pane would look like:

    tmux new -s tail_log -d
    tmux split-window -v -t tail_log
    tmux send-keys -t tail_log:0.1 'echo "pane 1"' C-m

## Resources Consulted

- [spin.atomicobject.com][spin.atomicobject.com] : source a tmux config file once you've attached to the server
- [toastdriven.com][toastdriven.com] : full example, minimal explanation of bash based tmux scripting
- [alternate addressing method][alternate-addressing] : bash tmux scripting with alternate addressing method
- [gradual tmux intro][gradual-intro] : a more gradual introduction to tmux commands and general window manipulation

[spin.atomicobject.com]: https://spin.atomicobject.com/2015/03/08/dev-project-workspace-tmux/ "Source tmux config"
[toastdriven.com]: http://toastdriven.com/blog/2009/oct/09/scripting-tmux/ "scripting tmux example"
[alternate-addressing]: https://gist.github.com/swaroopch/728896 "alternate addressing method"
[gradual-intro]: http://www.whiteboardcoder.com/2015/01/tmux-scripting.html "gradual tmux intro"
