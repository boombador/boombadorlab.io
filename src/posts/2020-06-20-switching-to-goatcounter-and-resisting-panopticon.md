---
title: BLM and Resisting the Digital Panopticon
date: 2020-06-20T00:00:00-04:00
layout: basic
draft: true
---

This post marks the replacement of Google Analytics with something called [GoatCounter](https://www.goatcounter.com/) for this blog. This alternative is [open source](https://github.com/zgoat/goatcounter) and attempts to minimize retained information beyond unique visitors and page views. This was motivated by a desire to avoid supporting the concentration of data in the hands of entities, such as Google, with the ability and incentive to abuse it. The granular tracking of browsing data and query history alone gives an alarming amount of insight into someone's behavior and motivations, to say nothing of other services such as analytics which only broaden the scope of collection. Example code for using with an SPA using hash history included at the bottom of this post.

However bringing my blog more into alignment with my views on data collection was itself prompted by a desire to say more. I want to commit to sharing my perspective authenticically and using this blog as a medium to engage with others.

I'm an inveterate lurker in most communities that I am a "part of". Part of this is a desire to avoid the baleful gaze of eternity, knowing that the casual thoughts or comments may become a liability later. With the permanence of distributed digital media, your ostensibly private interactions could be chopped up and sold on a marketplace to be used by either a disapproving government or public against you. Since public opinion could shift in ways that now seem dramatic, is it worth the risk to put your unadulterated opinions out there?

But I'm grateful for those who do take the risk, which points out to me that I am letting others bear the burden. I have security unavailable to many that would let me bear lesser risks than others and yet I am still prioritizing my own self-fortification. I always assumed that being silent was the majority position given the costs involved, and perhaps is is. But I want to stand for the beliefs that I do have so that I can actively develop them, changing if necessary, and augmenting the good they can provide.

I hope this isn't gratuitous or self-flagellating. The recent protests have made me feel as if I had been asleep, aware of racism but shrugging my shoulders due to lack of a clear guide to guaranteed progress.

I believe people have fundamentally the same hardware, and could behave roughly the same given equal initial conditions. This has dangers, in that it's possible to glean the workings of that machinery and understand what makes people tick. Through machine learning and widespread tracking it's conceivable that someday we could predict or suggest behavior pretty effectively and precisely, which could be a terrifying agent of control and power consolidation. Except without regulation in some form, these models will be only be available to those who created them.

However the similarity of the human machinery also has its benefits. We can cut each other some slack, recognizing that we would probably make similar mistakes given the same challenges. The difficulties of policing deserve affordances that recognize the dangerous situations it involves, but also accounts for the fallout that stress may have on citizens. And the Black communities that are historically under-resourced and over-policed may have something to do with statistics that appear to disagree with the human similarity hypothesis.

I'm planning to research more about how to civically engage with topics important to me on long-term way, starting  with racial justice since I wasn't focused on much aside from myself anyway.
