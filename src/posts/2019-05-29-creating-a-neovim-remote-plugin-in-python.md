---
started: 2019-05-29T11:36:30-0400
title: Replacing Vimscript with a NeoVim Remote Plugin in Python
language: python
---

Vim is a text editor that supports fantastic customization of your experience, but an unintuitive API can
obscure that flexibility for new users. Fortunately for fans of vim who would prefer to minimize its scripting there
is an alternative in the Neovim project. It has the same interface but supports writing plugins in your
favorite language, lowering the barriers to advanced vim functionality.

This post starts with a reconstructed history of scripting support in vim and how neovim fits in. Then we
walk through a tutorial on creating a python3 plugin for Neovim's remote plugin API. We'll call the
plugin `vim-tracker` and my version will be [available online][post-code].

Much of the information and code in this post applies for both neovim and vim 8, but was tested only
on neovim. Use the editor's `:help` command to view bundled documentation for your current version.

# Overview of apis

Scripting started out simply as files to collect frequent option settings.  Features like functions
accumulated over time and eventually became known as VimScript or VimL. There are several methods for extending
vim functionality which I'll briefly outline since the terminology can depend on the context.

While I currently prefer to work with the neovim's remote plugins, understanding the historical
systems will give insight into a more recent approaches. You may be constrained by your environment, wanting
to make use of existing code, or you may simply prefer vim. The original project has stood the test of time,
and I think any awkwardness in the original APIs can be explained by heroic efforts to maintain backwards compatibility.

## Runtimepath

Vim and neovim have a special setting called `runtimepath` that configures where the editor looks
for vimscript files to execute at startup. The runtimepath, abbreviated `rtp`, is a comma separated
list of directories that will be searched in order for vimscript files fitting certain naming
conventions.

You can think of vim searching all these paths as if it were merging the locations on the
runtimepath into one effective configuration directory. An excerpt from the help docs gives some
examples of what files and directories may be included at these configuration locations:

    filetype.vim    filetypes by file name |new-filetype|
    autoload/       automatically loaded scripts |autoload-functions|
    doc/            documentation |write-local-help|
    plugin/         plugin scripts |write-plugin|
    syntax/         syntax files |mysyntaxfile|

You can see your current runtimepath with `:set rtp?`, and `~/.config/nvim` will usually be early in
the list. That would be the canonical place to put your runtime files in neovim, but it is more
common to separate plugin functionality into git repositories and add each to your rtp via a plugin
manager.

## Original Vim Plugins

The vim help documentation describes a plugin as "a Vim script file that is loaded automatically
when Vim starts". Since there are many such files, I thought the guide for writing a plugin (also
found in the help documentation) better described plugins as when you "write a Vim script in such a way
that many people can use it".

Vim plugins refer to individual vimscript files and in the strictest sense specifically those under
the `plugin/` directory of the loaded config. But the term is sometimes used with other
automatically loaded vim scripts and more recently can refer to functionality or the collection of
files in a project repository.

```vim
:help vimrc-intro
:help write-plugin
```

## Synchronous Python API

Later vim added support for python and provided commands like `:python`, `:pyfile` to execute code
in snippets or files respectively. Eventually `python3` commands were added and a python interpreter
could even be bundled into the executable according to a compilation flag. These days most vim
distributions I encounter include `python2` by default.

This was a large step forward in flexibility for scripting in vim, except the python code would run
synchronously in the same process as the UI meaning it would become unresponsive while your plugin
code was running. My understanding is that the neovim project largely grew out of a desire to
rewrite some code in order to support running processes in a non-blocking fashion, but the code
forked into a new project instead.

Relevant help sections:

```vim
:help python-vim
:help python3
```

## Neovim's Remote Plugin API

Neovim's remote plugin API, which we will be focusing on in this post, extends the
idea of running external python scripts to arbitrary language runtimes in a non-blocking manner. It
achieves this by using the build process to expose the internal C functions for a remote procedure
call API based on the [MessagePack] serialization format.

The protocol is descriptively called MessagePack-RPC. This API supports several transport mechanisms
including TCP/IP and stdin/stdout, and can be used by any language that has the ability to use that
protocol.

Many languages now have [libraries][plugin-api-clients] that provide a "remote plugin host", which
is an environment for plugins that takes care of protocol communication on the language and
vimscript side. At the time of this writing 26 clients were supported.

Internally, NeoVim uses the job control API to run external processes such as the language specific
plugin hosts (commonly called language hosts) or other applications without blocking its own
execution. It also handles communication through the channel abstraction.

Relevant help sections:

```vim
:help remote-plugin
:help msgpack-rpc
:help rpc-api
:help job-control
:help channel
```

## Vim 8 Packages

Vim 8 added the concept of packages, which are a way of grouping files together in a style that is
backwards-compatible with the popular [pathogen] project. I won't explain the package system since I
don't use it, but this [blog post][vim-packages] gave me a helpful overview.

## Vim 8 Asynchronous Jobs

Vim 8 also added asynchronous jobs. I am not familiar with this API but you should know it exists to
avoid confusion since it also uses the term "job" in relevant keywords. If you need help telling
whether some vimscript is targeted at neovim or vim 8+ one hint is to look at the command used to
start a job, for neovim it's `jobstart` while in vim 8+ it's `job_start`.
 
It's unfortunate the APIs are confusingly similar, but if the original vim approach to jobs is more
backwards compatible then I'm at least glad asynchronous jobs have come to vim.

# Establishing our Development Environment

## Installing our Python Dependency

Neovim will need to be configured so that it has access to a python runtime with the `pynvim`
package available. It seems that this package was previously named `neovim` with a project name of
`python-client` on github, but the rename allows consistent identifiers without ambiguity between
the editor and the python client.

My preferred approach to configure python is currently to use [pyenv] along
with the [pyenv virtualenv] plugin. Here is an example of using them to configure python 3.7.3 for
use with neovim:

```bash
pyenv install 3.7.3
pyenv virtualenv 3.7.3 python3-venv
pyenv active python3-venv
pip install pynvim
```
Relevant help sections:

```vim
:help provider-python
```

### Verifying with the Python REPL

Let's check that python is properly configured. Activate your virtual environment if
you haven't already and start neovim with a named socket address:

    NVIM_LISTEN_ADDRESS=/tmp/nvim nvim

Now you are able to attach to that socket via the `pynvim` module. To get a REPL going open another
terminal and start `python` with that same virtualenv from earlier active. Then enter the following
statements based on the project's README sample code to print the contents of your nvim instance.

    >>> from pynvim import attach
    >>> nvim = attach('socket', path='/tmp/nvim')
    >>> buffer = nvim.current.buffer
    >>> buffer_contents = str.join('\n', buffer[:])
    >>> print(buffer_contents)

If you run into problems you can run the `:checkhealth` command to determine whether you have
satisfied the necessary dependencies. 

## Minimal Plugin

Now let's walk through setting up a simple plugin to show what's involved in development. This
plugin code will not do anything useful yet but we'll replace it with something more interesting
once we have our files in place.

### Plugin File Structure

We will create a directory at `~/Code/vim-tracker` to contain all our files, making it easier
to use version control and eventually share our new functionality.

```bash
mkdir -p ~/Code/vim-tracker
```

Now we add our first file for our python3 plugin. The `rplugin` directory holds files relevant to
neovim's remote plugins, and inside it are directories for each language host named for the language
they are written in.

```bash
mkdir -p ~/Code/vim-tracker/rplugin/python3
touch ~/Code/vim-tracker/rplugin/python3/sample.py
```

### Detecting the Plugin

We'll need to to let neovim know to look for these files, which we can achieve by adding the base
directory of the plugin to the runtimepath. To do that add the following line to your vimrc or
run as a vim command.

```vimscript
let &runtimepath = '~/Code/vim-tracker,' . &runtimepath
```

However even if `sample.py` contained valid plugin code at this point it wouldn't be considered
installed until we run `:UpdateRemotePlugins` in a later step.

### Sample Plugin Source

Now let's add the contents for `sample.py`, taken from the [pynvim docs][pynvim-docs-rplugins]. This
toy example doesn't do much but it demonstrates how you create a python class decorated with
`pynvim` functions to define a plugin.

    import pynvim

    @pynvim.plugin
    class TestPlugin(object):

        def __init__(self, nvim):
            self.nvim = nvim

        @pynvim.function('TestFunction', sync=True)
        def testfunction(self, args):
            return 3

        @pynvim.command('TestCommand', nargs='*', range='')
        def testcommand(self, args, range):
            self.nvim.current.line = ('Command with args: {}, range: {}'
                                    .format(args, range))
            self.nvim.out_write('Replaced current line with invocation summary\n')

        @pynvim.autocmd('BufEnter', pattern='*.py', eval='expand("<afile>")', sync=True)
        def on_bufenter(self, filename):
            self.nvim.out_write('TestPlugin is active while you\'ve entered file: ' + filename + '\n')

### Installing by Updating Remote Plugins

When we added the project to the runtimepath we only let vim know that a python3 plugin existed,
there is still another step to tell neovim which events should trigger the plugin. This is performed
in the `UpdateRemotePlugins` command, which starts up temporary language hosts to inspect the plugin
files and generate the necessary bootstrapping vimscript.

    :UpdateRemotePlugins

After entering the above command from within neovim the contents of `~/.local/share/nvim/rplugin.vim`
should looke like:

    call remote#host#RegisterPlugin('python3', '~/Code/vim-tracker/rplugin/python3/sample.py', [
        \ {'sync': v:true, 'name': 'BufEnter', 'type': 'autocmd', 'opts': {'pattern': '*.py', 'eval': 'expand("<afile>")'}},
        \ {'sync': v:false, 'name': 'TestCommand', 'type': 'command', 'opts': {'nargs': '*', 'range': ''}},
        \ {'sync': v:true, 'name': 'TestFunction', 'type': 'function', 'opts': {}},
        \ ])

The `remote#host#RegisterPlugin` function starts the language host if necessary and then passes off the
data to the specified remote plugin file based on the listed events.

An important note! Running `UpdateRemotePlugins` only updates this vimscript that defines what data
is sent to the remote host for which events, so if the host is already running then code changes in
your plugin won't be reflected. Currently you need to shutdown Neovim to stop the host and allow it
to be restarted when triggered by an event after starting neovim again.

Effectively this means you may not need to run `UpdateRemotePlugins` unless you change a plugin
method signature, but you will likely need to restart neovim if you want to see changes take effect.
Therefore it can be worthwhile testing core plugin logic using the language's native testing
tools.

### Minimal Vimrc (Optional)

It can be useful to create a minimal vimrc so there is less logic to debug when you're fixing
errors. Let's make one containing just the runtimepath update:

```bash
echo "let &runtimepath = '~/Code/vim-tracker,' . &runtimepath" \
    > ~/Code/vim-tracker/dev-vimrc
```

Then use the `-u` option to set the initialization vimrc:

```bash
nvim -u ~/Code/vim-tracker/dev-vimrc
```

### Using a Plugin Manager (Optional)

A plugin manager helps download project files and add them to the runtimepath.  I currently prefer
[Plug] because it multitasks downloading updates, making it quite fast, plus it has a simple
interface.

Install Plug for neovim by downloading a vimscript file into the project recommended location: the
`autoload/` subdirectory of `~/.local/share/nvim/site`, which is also on the runtimepath. We could
use `~/.config/nvim/autoload` instead, but I like to stick with defaults in case debugging is later
necessary.

```bash
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

Then add the following lines to your vimrc to let Plug know about your sample code, replacing the
previous `let &runtimepath = ...` line.

```vim
call plug#begin('~/.vim/plugged-dev')
Plug '~/Code/vim-tracker'
call plug#end()
```

Once you pushed this to a remote repository you would update the string to replace the filesystem
path with the git address, or github shorthand like with my version: 

```vim
Plug 'boombador/vim-tracker'
```

Now to actually install you run `:PlugInstall`.

# Example Plugin: An Event Tracker

Let's create a plugin to append messages to a logfile with a timestamp. In a new
file called `tracker.py` add the following unimplemented plugin class that we
will fill in as we go:

```python
import datetime
import os
import pynvim

@pynvim.plugin
class Tracker(object):

	def __init__(self, nvim):
		pass

	def _initialize(self):
		pass

	@pynvim.command('TrackerEvent', nargs='1', range='')
	def command_event(self, args, range):
		pass

	@pynvim.command('TrackerToggle', nargs='*', range='')
	def command_toggle(self, args, range):
		pass
```

You can run `:UpdateRemotePlugins` at this point and should not need to run it again since we won't
be changing any of the method signatures, but we will need to restart neovim once we've filled out
these functions. Let's start with `__init__` as we review each of the plugin methods.

```python
	def __init__(self, nvim):
		self.nvim = nvim
		self.initialized = False
```

The `__init__` method is passed a handle to the neovim session that triggered it and saved for later
use. The `initialized` attribute lets us defer any significant
initial computation to a helper method `_initialize`. This pattern is recommended by some [pynvim
documentation][pynvim-plugin-notes]:

> Plugins must not invoke API methods in __init__ or global module scope (or really do anything with
> non-trivial side-effects). A well-behaved rplugin will not start executing until its functionality
> is requested by the user.

The `_initialize` method handles the logic needed for plugin features to work. Any code that
depends on this logic will call it before executing, but it will only perform the initialization
logic the first time it is called. 

```python
	def _initialize(self):
		if not self.initialized:
			# potentially expensive initial logic goes here
			self.working = False
			self.root = '~/tracking'
			with open(os.path.expanduser('{}/toggles.txt'.format(self.root)), 'r') as f:
				lines = f.readlines()
				if len(lines) > 0:
					last_line = lines[-1]
					first_word = str.split(last_line, ' ')[0]
					if first_word == 'started':
						self.working = True
			self.initialized = True
```

The `root` attribute contains the directory where we'll store our tracking log files needed for any
specific features. The `working` boolean will be used for a later command, but this code parses one
of the files where messages are logged to determine whether the last written line indicates that the
plugin should start in the `working` state or not.

## TrackerEvent Command

First let's implement the `TrackerEvent` command, which will accept a message argument and append a
timestamped log line to `events.txt` under the configured root directory. The `pynvim.command`
decorator converts command arguments into an array and also performs error checking depending on the
value of the `nargs` argument.

```python
	@pynvim.command('TrackerEvent', nargs='1', range='')
	def command_event(self, args, range):
		self._initialize()
		message = args[0]
		log_line = '{} : {}\n'.format(datetime.datetime.now(), message)
		with open(os.path.expanduser('{}/events.txt'.format(self.root)), 'a') as f:
			f.write(log_line)
		self.nvim.out_write(log_line)
```

Before we enter our own plugin method, first the decorator handles formatting the event data into the
method arguments. `nargs` with argument to `'1'` sets the `args` parameter to the raw string
after the command name, which is more convenient than using quotation marks to make a single string
argument that includes spaces. A value of `'0'` would generate an error if an argument was passed.

First we call `self._initialize()` to make sure any necessary state is established before combining
the current datetime with the message argument. Then we write that timestamped message to the log
file at `~/tracking/events.txt`. We also write the message at the bottom of the vim screen as visual
confirmation.

## TrackerToggle Command

Now let's move onto the `TrackerToggle` command, which relies on the `working` boolean
to track whether you are currently active on some project over a series of calls. We saw in the
initialization function how the value was initially determined from the last line of the
`toggles.txt` log, now this function flips the value after each call logs its status update.

```python
    @pynvim.command('TrackerToggle', nargs='*', range='')
    def command_toggle(self, args, range):
        self._initialize()
        message = str.join(' ', args)
        now = datetime.datetime.now()
        action = 'stopped' if self.working else 'started'
        log_line = '{} at {} - {}\n'.format(action, now, message)
        with open(os.path.expanduser('{}/toggles.txt'.format(self.root)), 'a') as f:
            f.write(log_line)
        self.nvim.out_write(log_line)
        self.working = not self.working
```

The command also accepts a message that will be included with the timestamp and the status of
the `working` toggle. We will demonstrate the other `nargs` value `'*'` which breaks up the command
line according to word boundaries. Using `nargs='1'` would have also worked but it would
require an argument, using a variable number of arguments lets the parameter be optional.

# Next Steps

Our example plugin has several rough edges, but hopefully it gives you an idea of how you can 
use your favorite language for text-editing problems. For something that developers
spend so much time doing, reducing the friction to customizing your experience can pay dividends in
the long run.

Here are some potential improvements for our sample project to give you an idea of
what kind of things you can accomplish. Happy hacking!

- configure functionality with vimrc settings
- explore behavior of `range`
- calculate total time working
- include working indicator on statusline

[neovim]: https://neovim.io/
[pynvim]: https://github.com/neovim/pynvim
[pynvim-docs]: https://pynvim.readthedocs.io/en/latest/index.html
[pynvim-docs-rplugins]: https://pynvim.readthedocs.io/en/latest/usage/remote-plugins.html
[post-code]: https://github.com/boombador/tracker-vim
[pyenv]: https://github.com/pyenv/pyenv
[pyenv virtualenv]: https://github.com/pyenv/pyenv-virtualenv
[MessagePack]: https://msgpack.org/
[Plug]: https://github.com/junegunn/vim-plug
[vim-packages]: https://shapeshed.com/vim-packages/
[pathogen]: https://github.com/tpope/vim-pathogen
[plugin-api-clients]: https://github.com/neovim/neovim/wiki/Related-projects#api-clients
[pynvim-plugin-notes]: https://github.com/neovim/pynvim/blob/2165d6b6edd75b86c4763475d57d48a1bef8e2f9/docs/usage/remote-plugins.rst
[centos-nvim-install-guide]: https://jdhao.github.io/2018/12/24/centos_nvim_install_use_guide_en/
