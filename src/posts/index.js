// figure out how to load directory dynamically
import streamsAndRedirectionBash from "./2014-09-20-streams-and-redirection-bash.md";
import perlinNoiseDisplacementShader from "./2015-09-13-perlin-noise-displacement-shader";
import installNodeOnMint from "./2016-03-10-install-node-on-mint.md";
import tmuxScripting from "./2016-03-15-tmux-scripting.md";
import particleSystemsWithThreejs from "./2016-06-04-particle-systems-with-threejs.md";
import sheetMusicTrainingLibrary from "./2016-09-05-sheet-music-training-library.md";
import productivityAndProgrammingTools from "./2017-02-12-productivity-and-programming-tools.md";
import creatingArtWithSamplingAndBlenderScripting from "./2019-05-01-creating-art-with-sampling-and-blender-scripting.md";
import creatingANeovimRemotePluginInPython from "./2019-05-29-creating-a-neovim-remote-plugin-in-python.md";
import switchingFromGAandPanopticon from "./2020-06-20-switching-to-goatcounter-and-resisting-panopticon.md";

export default [
  streamsAndRedirectionBash,
  perlinNoiseDisplacementShader,
  installNodeOnMint,
  tmuxScripting,
  particleSystemsWithThreejs,
  sheetMusicTrainingLibrary,
  productivityAndProgrammingTools,
  creatingArtWithSamplingAndBlenderScripting,
  creatingANeovimRemotePluginInPython,
  switchingFromGAandPanopticon,
].reverse();
