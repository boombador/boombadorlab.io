import React from "react";
import { useLocation } from "react-router-dom";

import allPosts from "@/posts";
import { shouldIncludeDrafts } from "@/util";

import PostPreview from "@/components/PostPreview";

export default () => {
  // react router state isn't including search (because hash is at end?)
  // ideally should use:
  // const location = useLocation();
  const location = window.location;

  const includeDraft = shouldIncludeDrafts(location);
  const posts = allPosts.filter((post) => !post.draft || includeDraft);
  return (
    <main>
      <div className="home">
        {posts.map((post) => (
          <PostPreview key={post.title} {...post} />
        ))}
      </div>
    </main>
  );
};
