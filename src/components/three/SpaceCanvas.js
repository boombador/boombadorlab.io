import React from "react";
import { Canvas } from "react-three-fiber";
import styled from "styled-components";

import Planetoid from "./Planetoid";

const cameraStartLocation = [0, 151, 0];
const obeliskLocation = [-20, 160, -20];

const CanvasWrapper = styled.div`
  background-color: black;
  height: 200px;
`;

// old canvas markup
//<canvas id="headerCanvas"></canvas>
// id="headerCanvas"

export default ({ width, height }) => (
  <CanvasWrapper>
    <Canvas
      camera={{
        position: cameraStartLocation,
        aspect: width / height,
      }}
    >
      <Planetoid position={[0, 0, 0]} color={0xffffff} radius={150} />
    </Canvas>
  </CanvasWrapper>
);
