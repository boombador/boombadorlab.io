export const Obelisk = ({ position }) => (
  <mesh position={position}>
    <sphereGeometry attach="geometry" args={[1, 12, 12]} />
    <meshBasicMaterial attach="material" color="0x7851A9" flatShading="true" />
  </mesh>
);
