import React from "react";

const Planetoid = ({ position, color = 0xfff, radius = 1, segments = 16 }) => (
  <mesh position={position}>
    <sphereGeometry attach="geometry" args={[radius, segments, segments]} />
    <meshBasicMaterial attach="material" color={color} flatShading="true" />
  </mesh>
);

export default Planetoid;
