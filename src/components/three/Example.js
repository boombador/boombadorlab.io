export const Example = ({ vertices, color }) => (
  <group ref={ref => console.log('we have access to the instance')}>
    <line>
      <geometry
        attach="geometry"
        vertices={vertices.map(v => new THREE.Vector3(...v))}
        onUpdate={self => (self.verticesNeedUpdate = true)}
      />
      <lineBasicMaterial attach="material" color="black" />
    </line>
    <mesh 
      onClick={handleEvent} 
      onPointerOver={handleEvent} 
      onPointerOut={handleEvent}
    >
      <octahedronGeometry attach="geometry" />
      <meshBasicMaterial attach="material" color="peachpuff" opacity={0.5} transparent />
    </mesh>
  </group>
);
