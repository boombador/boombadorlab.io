import React from "react";
import styled from "styled-components";
import { HashRouter as Router, Route, Link } from "react-router-dom";

import Post from "@/components/Post";
import Home from "@/components/Home";

export default ({ children }) => (
  <Router>
    <div id="react-app" className="container">
      <header>
        <h1>
          <Link to="/">projective mindspace</Link>
        </h1>
        <p>a digital piece of my mind</p>
      </header>
      <Route path="/" component={Home} exact />
      <Route path="/post" component={Post} />
    </div>
  </Router>
);
