import React from "react";
import { Link } from "react-router-dom";

import { permalink } from "@/util";
import PostHeader from "./PostHeader";

export default (post) => {
  const link = permalink(post) + (post.draft ? "?drafts=assimilated" : "");

  return (
    <div className="blog-post">
      <PostHeader>
        <Link to={link}>{post.title}</Link>
      </PostHeader>
      <div dangerouslySetInnerHTML={{ __html: post.preview }} />
    </div>
  );
};
