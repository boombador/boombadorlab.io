import React from "react";
import { Link } from "react-router-dom";

import allPosts from "@/posts";
import { permalink, shouldIncludeDrafts } from "@/util";

import PostHeader from "./PostHeader";

const PostBody = (post) => {
  return (
    <main>
      <PostHeader>
        <Link to={permalink(post)}>{post.title}</Link>
      </PostHeader>
      <div
        className="post-body"
        dangerouslySetInnerHTML={{ __html: post.content }}
      />
    </main>
  );
};

export default ({ history, location, match }) => {
  // react router state isn't including search (because hash is at end?)
  // ideally should use:
  // const location = useLocation();
  //const location = window.location;
  const includeDraft = shouldIncludeDrafts(location);
  const currentLink = location.pathname;
  const isValidPost = (post) =>
    currentLink === permalink(post) && (includeDraft || !post.draft);
  const matchedPosts = allPosts.filter(isValidPost);
  if (matchedPosts.length < 1) {
    history.replace("/");
    return false;
  }
  return <PostBody {...matchedPosts[0]} />;
};
