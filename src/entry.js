import React from "react";
import ReactDOM from "react-dom";
import * as THREE from "three";
import marked from "marked";
import "normalize.css";
import "prismjs/themes/prism.css";

import "@/assets/main.scss";
import { initHeader } from "@/header";
import App from "@/components/App";
import initialLoadTracking from "@/lib/analytics";

if (redirectToHTTPS && window.location.protocol !== "https:") {
  // hacky, ideally this should be an HTTP (301 ?) redirect
  window.location = "https://" + window.location.host;
}

const mountReactOnPage = () => {
  // Render the HTML overlay that is meant to blend with the graphics
  const root = document.createElement("div");
  root.id = "root";
  document.body.appendChild(root);
  ReactDOM.render(<App />, document.getElementById("root"));
};

initHeader();
mountReactOnPage();
initialLoadTracking();

/* TODO:
 * more interesting camera paths, smooth tweening
 * other scene objects as foci for new pages
 */
