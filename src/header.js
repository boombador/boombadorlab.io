import * as THREE from "three";
import TWEEN from "@tweenjs/tween.js";
import { addEvent, queryWindowDraftsRequested } from "./util";

const cameraStartLocation = { x: 0, y: 151, z: 0 };
const obeliskLocation = { x: -20, y: 160, z: -20 };

const materials = {
  debug: new THREE.MeshBasicMaterial({ flatShading: true, color: 0xff0000 }),
  hovered: new THREE.MeshBasicMaterial({ flatShading: true, color: 0x00ff00 }),
  obelisk: new THREE.MeshBasicMaterial({ flatShading: true, color: 0x7851a9 }),
  planet: new THREE.MeshBasicMaterial({ flatShading: true, color: 0xffffff }),
};

const getWindowDimensions = () => ({
  width: window.innerWidth,
  height: window.innerHeight,
});

export const createRenderer = (canvas) => {
  const renderer = new THREE.WebGLRenderer({ canvas });
  renderer.setClearColor(0x000000);
  return renderer;
};

export const createCamera = () => new THREE.PerspectiveCamera(50, 1, 1, 10000);

export const handlePageResize = (renderer, camera) => {
  const { width, height } = getWindowDimensions();
  renderer.setSize(width, height);
  camera.aspect = width / height;
};

function makeParticleMaterial() {
  const particleUniforms = {
    color: { type: "c", value: new THREE.Color(0xffffff) },
  };

  const shaderMaterial = new THREE.ShaderMaterial({
    uniforms: particleUniforms,
    vertexShader: vertexshaderSource,
    fragmentShader: fragmentshaderSource,

    blending: THREE.AdditiveBlending,
    depthTest: false,
    transparent: true,
  });

  return shaderMaterial;
}

export const makeParticleSystem = (particleCount, radius) => {
  const geometry = new THREE.BufferGeometry();

  const positions = new Float32Array(particleCount * 3);
  const colors = new Float32Array(particleCount * 3);
  const sizes = new Float32Array(particleCount);

  const color = new THREE.Color();

  for (let i = 0, i3 = 0; i < particleCount; i++, i3 += 3) {
    positions[i3 + 0] = (Math.random() * 2 - 1) * radius;
    positions[i3 + 1] = (Math.random() * 2 - 1) * radius + radius;
    positions[i3 + 2] = (Math.random() * 2 - 1) * radius;

    color.setHSL(i / particleCount, 1.0, 0.5);

    colors[i3 + 0] = color.r;
    colors[i3 + 1] = color.g;
    colors[i3 + 2] = color.b;

    sizes[i] = 20;
  }

  geometry.addAttribute("position", new THREE.BufferAttribute(positions, 3));
  geometry.addAttribute("customColor", new THREE.BufferAttribute(colors, 3));
  geometry.addAttribute("size", new THREE.BufferAttribute(sizes, 1));

  const shaderMaterial = makeParticleMaterial();
  const particleSystem = new THREE.Points(geometry, shaderMaterial);

  return particleSystem;
};

export const vertexshaderSource = [
  "attribute float size;",
  "attribute vec3 customColor;",
  "varying vec3 vColor;",
  "void main() {",
  "vColor = customColor;",
  "vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );",
  "gl_PointSize = size * ( 30.0 / -mvPosition.z );",
  "gl_Position = projectionMatrix * mvPosition;",
  "}",
].join("\n");

export const fragmentshaderSource = [
  "uniform vec3 color;",
  "varying vec3 vColor;",
  "void main() {",
  "gl_FragColor = vec4( color * vColor, 1.0 );",
  "}",
].join("\n");

const getMaterial = (name) => {
  const mat = materials[name];
  return mat || materials.debug;
};

// apply location (array) to position
const setPositionFromObject = (position, { x, y, z }) => {
  position.set(x, y, z);
};

// Initialize the Three.js animations at the top of the page
export const initHeader = () => {
  const canvas = document.createElement("canvas");
  canvas.id = "headerCanvas";
  document.body.appendChild(canvas);

  let selectedPlanetoid = null;
  let tweenStarted = false;

  const renderer = createRenderer(canvas);
  const camera = createCamera();

  setPositionFromObject(camera.position, cameraStartLocation);
  handlePageResize(renderer, camera);

  // ray logic adapted from: https://threejs.org/docs/#api/en/core/Raycaster
  const raycaster = new THREE.Raycaster();
  const mouse = new THREE.Vector2();
  const scene = new THREE.Scene();

  const obelisk = new THREE.Mesh(
    new THREE.SphereGeometry(1, 12, 12),
    getMaterial("obelisk")
  );
  obelisk.name = "obelisk";
  setPositionFromObject(obelisk.position, obeliskLocation);
  scene.add(obelisk);

  const planetMesh = new THREE.Mesh(
    new THREE.SphereGeometry(150, 64, 64),
    getMaterial("planet")
  );
  planetMesh.name = "planet";
  scene.add(planetMesh);

  const particleSystem = makeParticleSystem(1000, 200);
  scene.add(particleSystem);

  const handleMousemove = (event) => {
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  };

  const handleClick = (event) => {
    if (!tweenStarted && selectedPlanetoid) {
      const includeDraftsCurrently = queryWindowDraftsRequested();
      if (includeDraftsCurrently) {
        window.location = "/";
      } else {
        window.location = "/?drafts=assimilated";
      }
      //const tween = new TWEEN.Tween(cameraStartLocation)
      //.to(obeliskLocation, 2000)
      //.onUpdate(elem => {
      //camera.position.set(elem.x, elem.y, elem.z);
      //const { x, y, z } = obeliskLocation;
      //const temp = createCamera();
      //temp.position.copy(camera.position);
      //temp.lookAt(x, y, z);
      //})
      //.start();
      //tweenStarted = true;
    }
  };

  addEvent(window, "resize", () => handlePageResize(renderer, camera));
  addEvent(window, "mousemove", handleMousemove); // originally: window.addEventListener( 'mousemove', onMouseMove, false );
  addEvent(window, "click", handleClick);

  function resetObjectColors() {
    for (let i = 0; i < scene.children.length; i++) {
      const obj = scene.children[i];
      if (obj.material.color) {
        obj.material = getMaterial(obj.name);
      }
    }
  }

  function validHoveredObject() {
    raycaster.setFromCamera(mouse, camera);
    const intersects = raycaster.intersectObjects(scene.children);

    // heighlight the intersection result collisions
    if (intersects.length > 0) {
      const obj = intersects[0].object;
      return obj;
    }
    return null;
  }

  function drawFrame(elapsedTime) {
    requestAnimationFrame(drawFrame);
    TWEEN.update(elapsedTime);

    resetObjectColors();

    raycaster.setFromCamera(mouse, camera);
    const intersects = raycaster.intersectObjects(scene.children);

    //const oldSelection = selectedPlanetoid;
    const obj = validHoveredObject();
    if (obj && obj.name === "obelisk") {
      selectedPlanetoid = obj;
      obj.material = getMaterial("hovered");
    } else {
      selectedPlanetoid = null;
    }

    camera.rotation.y += 0.001;
    renderer.render(scene, camera);
  }

  drawFrame();
};
