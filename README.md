Blog rendering code and posts

# Run Locally

Install your dependencies and run the script named 'start' to kick off the
development server:

    npm install  # only needed on first run
    npm start

This is a webpack devserver that will rebuild on javascript changes. The preview
server should be available at: http://localhost:9000

# Production build

Run `npm build` and look for production bundles in `dist/`.
