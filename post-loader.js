const getOptions = require('loader-utils').getOptions;
const validateOptions = require('schema-utils');
const marked = require('marked');
const { JSDOM } = require('jsdom');
const Prism = require('prismjs');

// WARNING: don't include this in webpack, apparently it pulls in a ton of languages
const loadLanguages = require('prismjs/components/');
loadLanguages(['bash', 'python', 'glsl']);

const schema = {
  type: 'object',
  properties: {
    test: {
      type: 'string'
    }
  }
};

const isTruthy = x => x;
const combineObjects = (a, b) => {
  return Object.assign({}, a, b);
};

const parsePreambleLine = line => {
  const [key, value] = line.split(':');
  return { [key]: value.trim() };
};

const parsePreamble = rawPreamble =>
  rawPreamble.split('\n')
    .map(x => x.trim())
    .filter(isTruthy)
    .map(parsePreambleLine)
    .reduce(combineObjects);

const extractPreview = markup => {
  const { window } = new JSDOM(markup);
  return window.document.body.firstChild.outerHTML;
};

function loader (source) {
  const [_, preamble, ...remainder] = source.split('---');
  const markdown = remainder.join('---');
  const metadata = parsePreamble(preamble);

  const options = {
    ...(getOptions(this) || {}), // is fallback necessary?
    highlight: function(code, languageTag) {
      const languageKey = languageTag || metadata.language;
      if (!languageKey || !(languageKey in Prism.languages)) {
        console.warn(`Language identifier unrecognized by prism: ${languageKey}`);
        return code;
      }
      const language = Prism.languages[languageKey];
      return Prism.highlight(code, language, languageKey); // what is third field?
    }
  };

  validateOptions(schema, options, 'Post Loader');

  // do I need this?: this.cacheable();
  marked.setOptions(options);

  const content = marked(markdown);
  const preview = extractPreview(content);

  const post = { ...metadata, markdown, content, preview, };
  return `export default ${ JSON.stringify(post) }`;
}

module.exports = loader;
